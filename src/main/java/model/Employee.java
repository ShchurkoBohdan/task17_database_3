package model;

import controller.EmployeeController;
import model.annotations.Column;
import model.annotations.PrimaryKey;
import model.annotations.Table;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Table(name = "employee")
public class Employee {
    public static Logger logger = LogManager.getLogger(Employee.class);
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "surname")
    private String surname;
    @Column(name = "gender")
    private String gender;

    public Employee(){}

    public Employee(String name, String surname, String gender) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
    }

    public Employee(int id, String name, String surname, String gender) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void print(){
        logger.info("\nID: " + getId() +
                       "\nName: " + getName() +
                       "\nSurname: " + getSurname() +
                       "\nGender: " + getGender()+
                       "\n====================");
    }
}
