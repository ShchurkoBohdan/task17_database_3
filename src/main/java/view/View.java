package view;

import controller.Controller;
import controller.EmployeeController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private Map<String, String> menu;
    private Map<String, Controller> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    public static Logger logger = LogManager.getLogger(View.class);

    public View() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        //menu.put("A", "   A - Select all table");
        //menu.put("B", "   B - Select structure of DB");

        menu.put("1", "   1 - Table: Employee");
        menu.put("11", "  11 - Create for Employee");
        menu.put("12", "  12 - Update Employee");
        menu.put("13", "  13 - Delete from Employee");
        menu.put("14", "  14 - Find Employee by ID");
        menu.put("15", "  15 - Find Employee by Name");
        menu.put("16", "  16 - Find Employee by Surname");
        menu.put("17", "  17 - Select all employees");

        menu.put("Q", "   Q - exit");

        methodsMenu.put("11", new EmployeeController()::createForEmployee);
        methodsMenu.put("12", new EmployeeController()::updateEmployee);
        methodsMenu.put("13", new EmployeeController()::deleteFromEmployee);
        methodsMenu.put("14", new EmployeeController()::findEmployeeByID);
        methodsMenu.put("15", new EmployeeController()::findEmployeeByName);
        methodsMenu.put("16", new EmployeeController()::findEmployeeBySurname);
        methodsMenu.put("17", new EmployeeController()::selectEmployee);
    }

    public void show() {
        String keyMenu = null;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();

            if (keyMenu.matches("^\\d")) {
                outputSubMenu(keyMenu);
                logger.info("Please, select menu point.");
                keyMenu = input.nextLine().toUpperCase();
            }

            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        logger.info("MENU:");
        for (String key : menu.keySet())
            if (key.length() == 1) logger.debug(menu.get(key));
    }

    private void outputSubMenu(String str) {

        logger.info("SubMENU:");
        for (String key : menu.keySet())
            if (key.length() != 1 && key.substring(0, 1).equals(str)) logger.debug(menu.get(key));
    }
}
