package dbconnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

public class DBconnection {
    public static Logger logger = LogManager.getLogger(DBconnection.class);
    public static final String URL = "jdbc:mysql://localhost:3306/employees_db";
    public static final String USER = "root";
    public static final String PASSWORD = "shchurko1";

    public static Connection connection = null;

    public static Connection getDBConnection() {
        if (Objects.isNull(connection)) {
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                connection = DriverManager.getConnection(URL, USER, PASSWORD);
                if (connection.isValid(100)) {
                    logger.trace("DB connection is set up successfully.");
                }
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("SQLException: " + e.getMessage());
                logger.error("SQLState: " + e.getSQLState());
                logger.error("VendorError: " + e.getErrorCode());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                logger.error("Exception: " + e.getMessage());
            }
        }
        return connection;
    }
}
