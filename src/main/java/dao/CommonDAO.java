package dao;

import java.sql.SQLException;
import java.util.List;

public interface CommonDAO<T, ID> {
    List<T> findAll() throws SQLException;
    T findById(ID id) throws SQLException;
    int create(T item) throws SQLException;
    int update(T item) throws SQLException;
    int delete(ID id) throws SQLException;

}
