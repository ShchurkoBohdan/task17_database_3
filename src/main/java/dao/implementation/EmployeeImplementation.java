package dao.implementation;

import dao.EmployeeDAO;
import dbconnection.DBconnection;
import dbdata.Converter;
import model.Employee;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeImplementation implements EmployeeDAO {
    private static final String CREATE = "INSERT INTO employee (name, surname, gender) VALUES (?, ?, ?)";
    private static final String UPDATE = "UPDATE employee SET name=?, surname=?, gender=? WHERE id=?";
    private static final String DELETE = "DELETE FROM employee WHERE id=?";
    private static final String FIND_ALL = "SELECT * FROM employee";
    private static final String FIND_BY_ID = "SELECT * FROM employee WHERE id=?";
    private static final String FIND_BY_FIRST_NAME = "SELECT * FROM employee WHERE name=?";
    private static final String FIND_BY_LAST_NAME = "SELECT * FROM employee WHERE surname=?";

    @Override
    public int create(Employee employee) throws SQLException {
        Connection conn = DBconnection.getDBConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setString(1, employee.getName());
            ps.setString(2, employee.getSurname());
            ps.setString(3, employee.getGender());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Employee employee) throws SQLException {
        Connection conn = DBconnection.getDBConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1, employee.getName());
            ps.setString(2, employee.getSurname());
            ps.setString(3, employee.getGender());
            ps.setInt(4, employee.getId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection conn = DBconnection.getDBConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }

    @Override
    public List<Employee> findAll() throws SQLException {
        List<Employee> employees = new ArrayList<>();
        Connection connection = DBconnection.getDBConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    employees.add((Employee) new Converter(Employee.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return employees;
    }

    @Override
    public Employee findById(Integer id) throws SQLException {
        Employee entity = null;
        Connection connection = DBconnection.getDBConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (Employee) new Converter(Employee.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    public List<Employee> findByName(String name) throws SQLException {
        List<Employee> employees = new ArrayList<>();
        Connection connection = DBconnection.getDBConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_FIRST_NAME)) {
            ps.setString(1, name);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    employees.add((Employee) new Converter(Employee.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return employees;
    }

    public List<Employee> findBySurname(String surname) throws SQLException {
        List<Employee> employees = new ArrayList<>();
        Connection connection = DBconnection.getDBConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_LAST_NAME)) {
            ps.setString(1, surname);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    employees.add((Employee) new Converter(Employee.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return employees;
    }
}
