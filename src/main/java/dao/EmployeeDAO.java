package dao;

import model.Employee;

public interface EmployeeDAO extends CommonDAO<Employee, Integer> {
}
