package service;

import dao.implementation.EmployeeImplementation;
import model.Employee;

import java.sql.SQLException;
import java.util.List;

public class EmployeeService {

    public int create(Employee employee) throws SQLException {
        return new EmployeeImplementation().create(employee);
    }

    public int update(Employee employee) throws SQLException {
        return new EmployeeImplementation().update(employee);
    }

    public int delete(Integer id) throws SQLException {
        return new EmployeeImplementation().delete(id);
    }

    public List<Employee> findAll() throws SQLException {
        return new EmployeeImplementation().findAll();
    }

    public Employee findById(Integer id) throws SQLException {
        return new EmployeeImplementation().findById(id);
    }

    public List<Employee> findByName(String name) throws SQLException {
        return new EmployeeImplementation().findByName(name);
    }

    public List<Employee> findBySurname(String surname) throws SQLException {
        return new EmployeeImplementation().findBySurname(surname);
    }
}
