package controller;

import model.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.EmployeeService;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class EmployeeController {
    public static Scanner scanner = new Scanner(System.in);
    public static Logger logger = LogManager.getLogger(EmployeeController.class);

    public void createForEmployee() throws SQLException {
        logger.info("Input Name for Employee: ");
        String name = scanner.nextLine();
        logger.info("Input Surname for Employee: ");
        String surname = scanner.nextLine();
        logger.info("Input Gender for Employee: ");
        String gender = scanner.nextLine();
        Employee employee = new Employee(name, surname, gender);
        EmployeeService employeeService = new EmployeeService();

        int count = employeeService.create(employee);
        logger.info(String.format("There are created %d rows\n", count));
    }

    public void updateEmployee() throws SQLException {
        logger.info("Input ID for Employee: ");
        Integer id = scanner.nextInt();
        scanner.nextLine();
        logger.info("Input Name for Employee: ");
        String name = scanner.nextLine();
        logger.info("Input Surname for Employee: ");
        String surname = scanner.nextLine();
        logger.info("Input Gender for Employee: ");
        String gender = scanner.nextLine();
        Employee employee = new Employee(id, name, surname, gender);
        EmployeeService employeeService = new EmployeeService();

        int count = employeeService.update(employee);
        logger.info(String.format("There are updated %d rows\n", count));
    }

    public void deleteFromEmployee() throws SQLException {
        logger.info("Input ID for Employee: ");
        Integer id = scanner.nextInt();
        scanner.nextLine();
        EmployeeService employeeService = new EmployeeService();
        int count = employeeService.delete(id);
        logger.info(String.format("There are deleted %d rows\n", count));
    }



    public void selectEmployee() throws SQLException {
        logger.info("\nTable: Employee");
        EmployeeService employeeService = new EmployeeService();
        List<Employee> employees = employeeService.findAll();
        for (Employee employee : employees) {
            employee.print();
        }
    }

    public void findEmployeeByID() throws SQLException {
        logger.info("Input ID for Employee: ");
        Integer id = scanner.nextInt();
        scanner.nextLine();
        EmployeeService employeeService = new EmployeeService();
        Employee employee = employeeService.findById(id);
        employee.print();
    }

    public void findEmployeeByName() throws SQLException {
        logger.info("Input Name for Employee: ");
        String name = scanner.nextLine();
        EmployeeService employeeService = new EmployeeService();
        List<Employee> employees = employeeService.findByName(name);
        for (Employee employee : employees) {
            //logger.info(employee);
            employee.print();
        }
    }

    public void findEmployeeBySurname() throws SQLException {
        logger.info("Input Surname for Employee: ");
        String surname = scanner.nextLine();
        EmployeeService employeeService = new EmployeeService();
        List<Employee> employees = employeeService.findBySurname(surname);
        for (Employee employee : employees) {
            employee.print();
        }
    }
}
