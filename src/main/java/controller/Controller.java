package controller;

import java.sql.SQLException;

@FunctionalInterface
public interface Controller {
    void print() throws SQLException;
}
