create schema if not exists employees_DB;
use employees_DB;

create table if not exists employee (
  `id` int not null auto_increment primary key,
  `name` varchar(45) not null,
  `surname` varchar(45) not null,
  `gender` varchar(45)
 # foreign key (id) references employees_DB.salary(emp_id)
  )
engine = InnoDB;

insert into employee (name, surname, gender)
values ('Igor', 'Turko', 'male'),
	   ('Andrii', 'Makarov', 'male'),
       ('Oksana', 'Andriiv', 'female');

create table if not exists salary (
  `id` int not null auto_increment primary key,
  `salary` int not null,
  `emp_id` int,
  constraint `fk_employee_id`
  foreign key (emp_id) references employee(id)
  ON DELETE NO ACTION ON UPDATE NO ACTION
  )
  engine = InnoDB;

insert into salary (salary, emp_id)
values (1550, 1),
	   (1800, 2),
       (2000, 3);
       
alter table employee 
add constraint `fk_id`
foreign key (id) references salary(emp_id);